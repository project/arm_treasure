<?php

/**
 * @file
 * Contains arm_treasure.module.
 */

use Drupal\Core\Render\Markup;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Attach Arm Treasure Javascript SDK to Page.
 *
 * @param array $page
 *   Attach data to page object.
 */
function arm_treasure_page_attachments(array &$page) {

  // get_arm_treasure configs.
  $config = \Drupal::config('arm_treasure.settings');
  $database = $config->get('arm_treasure_database');
  $api_key = $config->get('arm_treasure_api_key');
  $hostname = $config->get('arm_treasure_hostname');
  $pageviews = $config->get('arm_treasure_pageviews');
  $use_inline_javascript = $config->get('arm_treasure_use_inline_javascript');

  // Attach the JavaScript snipper to the <head/>.
  $page['#attached']['html_head'][] = [
      [
        '#type' => 'html_tag',
        '#tag' => 'script',
        '#value' => Markup::create($use_inline_javascript),
        '#attributes' => ['type' => 'text/javascript'],
        '#weight' => 9999999,
      ],
    'arm_treasure_inline_javascript',
  ];

  // Pass Arm Treasure settings to Javascript.
  $page['#attached']['drupalSettings']['arm_treasure']['database'] = $database;
  $page['#attached']['drupalSettings']['arm_treasure']['api_key'] = $api_key;
  $page['#attached']['drupalSettings']['arm_treasure']['hostname'] = $hostname;
  $page['#attached']['drupalSettings']['arm_treasure']['pageviews'] = $pageviews;

  $page['#attached']['library'][] = 'arm_treasure/arm_treasure_external';
}

/**
 * Implements hook_help().
 */
function arm_treasure_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Help page for Arm Treasure SDK.
    case 'help.page.arm_treasure':
      $output = "";
      $output .= "<h3>" . t("About") . "</h3>";
      $output .= "<p>" . t("Module to integrate with Arm Tresaure's JS-SDK.") . "</p>";
      return $output;

    default:
  }
}
