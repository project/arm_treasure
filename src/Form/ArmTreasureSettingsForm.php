<?php

namespace Drupal\arm_treasure\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form to hold Arm Treasure Settings.
 */
class ArmTreasureSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   array contains config file name.
   */
  protected function getEditableConfigNames() {
    return [
      'arm_treasure.settings',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @return string
   *   string containing thee form ID.
   */
  public function getFormId() {
    return 'arm_treasure_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   Drupalform object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form state.
   *
   * @return object
   *   Returns parent form object.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('arm_treasure.settings');

    $form['arm_treasure_database'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database'),
      '#description' => $this->t('Arm Treasure SDK database name.'),
      '#default_value' => $config->get('arm_treasure_database'),
      '#size' => "75",
    ];
    $form['arm_treasure_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('WriteKey'),
      '#description' => $this->t('Arm Treasure SDK Writekey.'),
      '#default_value' => $config->get('arm_treasure_api_key'),
      '#size' => "75",
    ];
    $form['arm_treasure_hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#description' => $this->t('Arm Treasure Hostname.'),
      '#default_value' => $config->get('arm_treasure_hostname'),
      '#size' => "75",
    ];
    $form['arm_treasure_pageviews'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pageviews'),
      '#description' => $this->t('Arm Treasure Pageview Table.'),
      '#default_value' => $config->get('arm_treasure_pageviews'),
      '#size' => "75",
    ];

    $form['arm_treasure_use_inline_javascript'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Arm Treasure SDK'),
      '#description' => $this->t('Copy & paste the <strong>Script snippet</strong> from https://github.com/treasure-data/td-js-sdk and remove the &lt;script&gt; tags before pasting.'),
      '#default_value' => $config->get('arm_treasure_use_inline_javascript'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   Drupal Formm Object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal Form State Object.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('arm_treasure.settings')
      ->set('arm_treasure_database', $form_state->getValue('arm_treasure_database'))
      ->set('arm_treasure_api_key', $form_state->getValue('arm_treasure_api_key'))
      ->set('arm_treasure_hostname', $form_state->getValue('arm_treasure_hostname'))
      ->set('arm_treasure_pageviews', $form_state->getValue('arm_treasure_pageviews'))
      ->set('arm_treasure_use_inline_javascript', $form_state->getValue('arm_treasure_use_inline_javascript'))
      ->save();
  }

  /**
   * Form validation for Arm Treasure Configuration Settings.
   *
   * @param array $form
   *   Drupal $form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Druapl $form_state object.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Database needs to be alphnumberic with the possibility of a '-'.
    preg_match('/[\w\d-]+$/', $form_state->getValue('arm_treasure_database'), $db_matches);
    $db_regular_expression_value = $db_matches[0];

    if (is_null($db_regular_expression_value)) {
      $form_state->setErrorByName('arm_treasure_database', $this->t('Please enter a valid database name.'));
    }

    if ($form_state->isValueEmpty('arm_treasure_api_key')) {
      $form_state->setErrorByName('arm_treasure_api_key', $this->t('Please enter a valid API Key.'));
    }

    if ($form_state->isValueEmpty('arm_treasure_hostname')) {
      $form_state->setErrorByName('arm_treasure_hostname', $this->t('Please enter a valid hostname.'));
    }

    if ($form_state->isValueEmpty('arm_treasure_pageviews')) {
      $form_state->setErrorByName('arm_treasure_pageviews', $this->t('Please enter a pageview entry.'));
    }

    if ($form_state->isValueEmpty('arm_treasure_use_inline_javascript')) {
      $form_state->setErrorByName('arm_treasure_use_inline_javascript', $this->t('Please paste your inline Javascript without the <script/> tags'));
    }
  }

}
