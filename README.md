CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration

## INTRODUCTION
The Arm Treasure module provides your site integration into the
Arm Treasure's JavaScript SDK.

## REQUIREMENTS
You need to have Drupal 8 installed and have your Arm Treasure
database, hostname and API Keys ready.

## INSTALLATION
- Download this module from Drupal.org and place it in 
your /modules/custom/ directory.
- Go to the /admin/modules and enable the Arm Treasure Module.

## CONFIGURATION
Go to `/admin/config/system/arm_treasure` to see your configuration settings.
Copy and paste the JavaScript SDK ( remove the `<script>` open 
and closing tags ) from https://github.com/treasure-data/td-js-sdk 
into the 'Arm Treasure SDK' textarea on the
module's configuration settings.

### INITIALIZING THE ARM TREASURE OBJECT IN JAVASCRIPT
There is an `arm_treasure.js` file in the `/js` directory that will 
initialize your treasure data object after the Arm Treasure JavaScript SDK
loads and you enter your configurations settings.
