/**
 * @file
 */

(function ($) {
  'use strict';
  Drupal.behaviors.arm_treasure = {
      attach: function (context, settings) {
        var td = new Treasure(
            {
                database: settings.arm_treasure.database,
                writeKey: settings.arm_treasure.api_key,
                host: settings.arm_treasure.hostname
            }
        );
        // Enable cross-domain tracking.
        td.set('$global', 'td_global_id', 'td_global_id');
        td.trackPageview(settings.arm_treasure.pageviews);
        // Add custom code.
      }
  };
}(jQuery));
